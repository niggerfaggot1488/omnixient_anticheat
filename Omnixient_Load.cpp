#include "Omnixient_Module.hpp"

using namespace::Omnixient;

int main() {
	OX_MODULE_INFO_VECTOR lpModules;
	if (GetModulesA(lpModules)) {
		for (int i(0); i < lpModules.size(); ++i) {
			OX_MODULE_INFO lpCurrentModule = lpModules.at(i);

#if defined(_UNICODE) && defined(UNICODE)
			wchar_t szModuleInfo[256];
			sprintf_s(szModuleInfo, "[%ws]\n0x%08X\n%dkb\n%ws",
				lpCurrentModule.szName.c_str(), lpCurrentModule.uiBaseAddress,
				lpCurrentModule.uiSize / 1024, lpCurrentModule.szPathToFile.c_str());
			printf("%ws\n\n", szModuleInfo);
#elif defined(_MBCS)
			char szModuleInfo[256];
			sprintf_s(szModuleInfo, "[%s]\n0x%08X\n%dkb\n%s",
				lpCurrentModule.szName.c_str(), lpCurrentModule.uiBaseAddress,
				lpCurrentModule.uiSize / 1024, lpCurrentModule.szPathToFile.c_str());
			printf("%s\n\n", szModuleInfo);
#endif
		}
	}

	getchar();

	return false;
}