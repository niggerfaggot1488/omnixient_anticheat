/////////////////////////////////////////////////////////////////////////////
//                                                                         //
//  ██████╗ ███╗   ███╗███╗   ██╗██╗██╗  ██╗██╗███████╗███╗   ██╗████████╗ //
// ██╔═══██╗████╗ ████║████╗  ██║██║╚██╗██╔╝██║██╔════╝████╗  ██║╚══██╔══╝ //
// ██║   ██║██╔████╔██║██╔██╗ ██║██║ ╚███╔╝ ██║█████╗  ██╔██╗ ██║   ██║    //
// ██║   ██║██║╚██╔╝██║██║╚██╗██║██║ ██╔██╗ ██║██╔══╝  ██║╚██╗██║   ██║    //
// ╚██████╔╝██║ ╚═╝ ██║██║ ╚████║██║██╔╝ ██╗██║███████╗██║ ╚████║   ██║    //
//  ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝    //
//                                                                         //
// Omnixient_Module.hpp                                                    //
// 31/10/18                                                                //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

#ifndef _Ox_Module_Hpp_
	#define _Ox_Module_Hpp_

#include <string>
#include <vector>
#include <stdlib.h>
#include <intrin.h>

#pragma region Win Structures

struct _IMAGE_DATA_DIRECTORY {
	unsigned int	VirtualAddress;
	unsigned int	Size;
};
using IMAGE_DATA_DIRECTORY = _IMAGE_DATA_DIRECTORY;
using PIMAGE_DATA_DIRECTORY = _IMAGE_DATA_DIRECTORY * ;

struct _IMAGE_OPTIONAL_HEADER {
	unsigned short			Magic;
	unsigned char			MajorLinkerVersion;
	unsigned char			MinorLinkerVersion;
	unsigned int			SizeOfCode;
	unsigned int			SizeOfInitializedData;
	unsigned int			SizeOfUninitializedData;
	unsigned int			AddressOfEntryPoint;
	unsigned int			BaseOfCode;
	unsigned int			BaseOfData;
	unsigned int			ImageBase;
	unsigned int			SectionAlignment;
	unsigned int			FileAlignment;
	unsigned short			MajorOperatingSystemVersion;
	unsigned short			MinorOperatingSystemVersion;
	unsigned short			MajorImageVersion;
	unsigned short			MinorImageVersion;
	unsigned short			MajorSubsystemVersion;
	unsigned short			MinorSubsystemVersion;
	unsigned int			Win32VersionValue;
	unsigned int			SizeOfImage;
	unsigned int			SizeOfHeaders;
	unsigned int			CheckSum;
	unsigned short			Subsystem;
	unsigned short			DllCharacteristics;
	unsigned int			SizeOfStackReserve;
	unsigned int			SizeOfStackCommit;
	unsigned int			SizeOfHeapReserve;
	unsigned int			SizeOfHeapCommit;
	unsigned int			LoaderFlags;
	unsigned int			NumberOfRvaAndSizes;
	IMAGE_DATA_DIRECTORY	DataDirectory[0x10];
};
using IMAGE_OPTIONAL_HEADER = _IMAGE_OPTIONAL_HEADER;
using PIMAGE_OPTIONAL_HEADER = _IMAGE_OPTIONAL_HEADER * ;

struct _IMAGE_NT_HEADERS {
	unsigned int			Signature;
	void*					FileHeader;
	IMAGE_OPTIONAL_HEADER	OptionalHeader;
};
using IMAGE_NT_HEADERS = _IMAGE_NT_HEADERS;
using PIMAGE_NT_HEADERS = _IMAGE_NT_HEADERS * ;

struct _IMAGE_EXPORT_DIRECTORY {
	unsigned int	Characteristics;
	unsigned int	TimeDateStamp;
	unsigned short	MajorVersion;
	unsigned short	MinorVersion;
	unsigned int	Name;
	unsigned int	Base;
	unsigned int	NumberOfFunctions;
	unsigned int	NumberOfNames;
	unsigned int	AddressOfFunctions;
	unsigned int	AddressOfNames;
	unsigned int	AddressOfNameOrdinals;
};
using IMAGE_EXPORT_DIRECTORY = _IMAGE_EXPORT_DIRECTORY;
using PIMAGE_EXPORT_DIRECTORY = IMAGE_EXPORT_DIRECTORY * ;

#pragma endregion

#pragma region NT Structures

struct _UNICODE_STRING
{
	unsigned short	Length;
	unsigned short	MaximumLength;
	wchar_t*		Buffer;
};
using UNICODE_STRING = _UNICODE_STRING;
using PUNICODE_STRING = _UNICODE_STRING * ;

struct _LIST_ENTRY
{
	struct _LIST_ENTRY	*Flink;
	struct _LIST_ENTRY	*Blink;
};
using LIST_ENTRY = _LIST_ENTRY;
using PLIST_ENTRY = _LIST_ENTRY * ;

struct _PEB_LDR_DATA
{
	unsigned long	Length;
	unsigned char	Initialized;
	void*			SsHandle;
	LIST_ENTRY		InLoadOrderModuleList;
	LIST_ENTRY		InMemoryOrderModuleList;
	LIST_ENTRY		InInitializationOrderModuleList;
	void*			EntryInProgress;
};
using PEB_LDR_DATA = _PEB_LDR_DATA;
using PPEB_LDR_DATA = _PEB_LDR_DATA * ;

struct _LDR_DATA_TABLE_ENTRY_BASE
{
	LIST_ENTRY		InLoadOrderLinks;
	LIST_ENTRY		InMemoryOrderLinks;
	LIST_ENTRY		InInitializationOrderLinks;
	void*			DllBase;
	void*			EntryPoint;
	unsigned long	SizeOfImage;
	UNICODE_STRING	FullDllName;
	UNICODE_STRING	BaseDllName;
	unsigned long	Flags;
	unsigned short	LoadCount;
	unsigned short	TlsIndex;
	LIST_ENTRY		HashLinks;
	unsigned long	TimeDateStamp;
	void*			EntryPointActivationContext;
	void*			PatchInformation;
};
using LDR_DATA_TABLE_ENTRY_BASE = _LDR_DATA_TABLE_ENTRY_BASE;
using PLDR_DATA_TABLE_ENTRY_BASE = _LDR_DATA_TABLE_ENTRY_BASE * ;

#pragma endregion

namespace Omnixient {
	struct OX_MODULE_INFOA {
#if defined(_WIN64) // x64
		std::string		szName;
		std::string		szPathToFile;
		unsigned long	uiBaseAddress;
		unsigned long	uiSize;
#else // x86
		std::string		szName;
		std::string		szPathToFile;
		unsigned int	uiBaseAddress;
		unsigned int	uiSize;
#endif
	};

	struct OX_MODULE_INFOW {
#if defined(_WIN64) // x64
		std::wstring	szName;
		std::wstring	szPathToFile;
		unsigned long	uiBaseAddress;
		unsigned long	uiSize;
#else // x86
		std::wstring	szName;
		std::wstring	szPathToFile;
		unsigned int	uiBaseAddress;
		unsigned int	uiSize;
#endif
	};

	using OX_MODULE_INFO_VECTORA = std::vector<OX_MODULE_INFOA>; // ANSI
	using OX_MODULE_INFO_VECTORW = std::vector<OX_MODULE_INFOW>; // UNICODE

#if defined(_UNICODE) && defined(UNICODE) // UNICODE
	#define OX_MODULE_INFO_VECTOR	OX_MODULE_INFO_VECTORW
	#define OX_MODULE_INFO			OX_MODULE_INFOW
#elif defined (_MBCS) // ANSI
	#define OX_MODULE_INFO_VECTOR	OX_MODULE_INFO_VECTORA
	#define OX_MODULE_INFO			OX_MODULE_INFOA
#endif

#if defined(_WIN64) // x64
	auto __forceinline GetLdrData_Internal(void) -> PEB_LDR_DATA* {
		return reinterpret_cast<PEB_LDR_DATA*>(__readgsqword(0x30) + 0x60 + 0x18);
	}
#else // x86
	auto __forceinline GetLdrData_Internal(void) -> PEB_LDR_DATA* {
		void* lpAddress = nullptr;

		__asm {
			mov eax, fs:[0x18];
			mov eax, [eax + 0x30];
			mov eax, [eax + 0xC];
			mov lpAddress, eax;
		}

		return reinterpret_cast<PEB_LDR_DATA*>(lpAddress);
	}
#endif

	auto GetModulesA(OX_MODULE_INFO_VECTORA& lpModules) -> bool {
		auto lpLdrData = reinterpret_cast<LDR_DATA_TABLE_ENTRY_BASE*>
			(GetLdrData_Internal()->InLoadOrderModuleList.Flink);

		if (lpLdrData) {
			do {
				char szModuleName[256];
				char szModulePath[256];

				// Convert to ANSI from UNICODE
				wcstombs_s(nullptr, szModuleName, lpLdrData->BaseDllName.Buffer,
					lpLdrData->BaseDllName.Length);
				wcstombs_s(nullptr, szModulePath, lpLdrData->FullDllName.Buffer,
					lpLdrData->FullDllName.Length);

				// Store information about module
				OX_MODULE_INFOA lpModuleInfo = decltype(lpModuleInfo){};
				lpModuleInfo.szName = std::string(szModuleName);
				lpModuleInfo.szPathToFile = std::string(szModulePath);
				lpModuleInfo.uiBaseAddress =
					reinterpret_cast<decltype(lpModuleInfo.uiBaseAddress)>
					(lpLdrData->DllBase);
				lpModuleInfo.uiSize = lpLdrData->SizeOfImage;

				// Add module to list
				lpModules.insert(lpModules.end(), lpModuleInfo);

				// Move to next module
				lpLdrData = reinterpret_cast<decltype(lpLdrData)>
					(lpLdrData->InLoadOrderLinks.Flink);
			} while (lpLdrData->DllBase);
		}

		if (lpModules.size() > 0) {
			return true;
		}

		else {
			return false;
		}
	}

	auto GetModulesW(OX_MODULE_INFO_VECTORW& lpModules) -> bool {
		auto lpLdrData = reinterpret_cast<LDR_DATA_TABLE_ENTRY_BASE*>
			(GetLdrData_Internal()->InLoadOrderModuleList.Flink);

		if (lpLdrData) {
			do {
				// Store information about module
				OX_MODULE_INFOW lpModuleInfo = decltype(lpModuleInfo){};
				lpModuleInfo.szName = std::wstring
				(lpLdrData->BaseDllName.Buffer);
				lpModuleInfo.szPathToFile = std::wstring
				(lpLdrData->FullDllName.Buffer);
				lpModuleInfo.uiBaseAddress =
					reinterpret_cast<decltype(lpModuleInfo.uiBaseAddress)>
					(lpLdrData->DllBase);
				lpModuleInfo.uiSize = lpLdrData->SizeOfImage;

				// Add module to list
				lpModules.insert(lpModules.end(), lpModuleInfo);

				// Move to next module
				lpLdrData = reinterpret_cast<decltype(lpLdrData)>
					(lpLdrData->InLoadOrderLinks.Flink);
			} while (lpLdrData->DllBase);
		}

		if (lpModules.size() > 0) {
			return true;
		}

		else {
			return false;
		}
	}

	auto FindModuleA(OX_MODULE_INFOA& lpModule, std::string lpModuleName) -> bool {
		if (!lpModuleName.empty()) {
			auto lpLdrData = reinterpret_cast<LDR_DATA_TABLE_ENTRY_BASE*>
				(GetLdrData_Internal()->InLoadOrderModuleList.Flink);

			if (lpLdrData) {
				do {
					char szModuleName[256];
					char szModulePath[256];

					// Convert to ANSI from UNICODE
					wcstombs_s(nullptr, szModuleName, lpLdrData->BaseDllName.Buffer,
						lpLdrData->BaseDllName.Length);
					wcstombs_s(nullptr, szModulePath, lpLdrData->FullDllName.Buffer,
						lpLdrData->FullDllName.Length);

					// If module name is a match (case insensitive)
					if (!_stricmp(szModuleName, lpModuleName.c_str()))
					{
						// Store information about module
						lpModule.szName = std::string(szModuleName);
						lpModule.szPathToFile = std::string(szModulePath);
						lpModule.uiBaseAddress =
							reinterpret_cast<decltype(lpModule.uiBaseAddress)>
							(lpLdrData->DllBase);
						lpModule.uiSize = lpLdrData->SizeOfImage;

						return true;
					}

					// Move to next module
					lpLdrData = reinterpret_cast<decltype(lpLdrData)>
						(lpLdrData->InLoadOrderLinks.Flink);
				} while (lpLdrData->DllBase);
			}
		}

		return false;
	}

	auto FindModuleW(OX_MODULE_INFOW& lpModule, std::wstring lpModuleName) -> bool {
		if (!lpModuleName.empty()) {
			auto lpLdrData = reinterpret_cast<LDR_DATA_TABLE_ENTRY_BASE*>
				(GetLdrData_Internal()->InLoadOrderModuleList.Flink);

			if (lpLdrData) {
				do {
					// If module name is a match (case insensitive)
					if (!_wcsicmp(lpLdrData->BaseDllName.Buffer,
						lpModuleName.c_str()))
					{
						// Store information about module
						lpModule.szName = std::wstring(lpLdrData->BaseDllName.Buffer);
						lpModule.szPathToFile =
							std::wstring(lpLdrData->FullDllName.Buffer);
						lpModule.uiBaseAddress =
							reinterpret_cast<decltype(lpModule.uiBaseAddress)>
							(lpLdrData->DllBase);
						lpModule.uiSize = lpLdrData->SizeOfImage;

						return true;
					}

					// Move to next module
					lpLdrData = reinterpret_cast<decltype(lpLdrData)>
						(lpLdrData->InLoadOrderLinks.Flink);
				} while (lpLdrData->DllBase);
			}
		}

		return false;
	}

	/* TODO: 

	auto GetFunctionA(OX_MODULE_INFOA& lpModule, std::string lpFunctionName) -> void* {
		if (!lpFunctionName.empty()) {
			void* lpAddressOfFunction = nullptr;

			void* hModule = reinterpret_cast<decltype(hModule)>(lpModule.uiBaseAddress);
			unsigned long ulModuleBytes = reinterpret_cast<decltype(ulModuleBytes)>
				(hModule);

			IMAGE_NT_HEADERS* lpNtHdr = reinterpret_cast<decltype(lpNtHdr)>
				(ulModuleBytes + 0xE8);
			IMAGE_EXPORT_DIRECTORY* lpExpDir = reinterpret_cast<decltype(lpExpDir)>
				(ulModuleBytes + 
					static_cast<unsigned long>
					(lpNtHdr->OptionalHeader.DataDirectory[0x0].VirtualAddress));

			unsigned long*	ulFunctions = reinterpret_cast<decltype(ulFunctions)>
				(ulModuleBytes + lpExpDir->AddressOfFunctions);
			unsigned long*	ulNames = reinterpret_cast<decltype(ulNames)>
				(ulModuleBytes + lpExpDir->AddressOfNames);
			unsigned short* uiOrdinals = reinterpret_cast<decltype(uiOrdinals)>
				(ulModuleBytes + lpExpDir->AddressOfNameOrdinals);

			for (long i(0); i < lpExpDir->NumberOfNames; ++i) {
				if (!strcmp(lpFunctionName.c_str(),
					reinterpret_cast<const char*>(hModule) + ulNames[i])) {
					lpAddressOfFunction = reinterpret_cast<decltype(lpAddressOfFunction)>
						(ulModuleBytes + ulFunctions[uiOrdinals[i]]);
					return lpAddressOfFunction;
				}
			}
		}

		return false;
	}

	auto GetFunctionW(OX_MODULE_INFOW& lpModule, std::wstring lpFunctionName) -> void* {
		if (!lpFunctionName.empty()) {
			void* lpAddressOfFunction = nullptr;

			void* hModule = reinterpret_cast<decltype(hModule)>(lpModule.uiBaseAddress);
			unsigned char* ulModuleBytes = reinterpret_cast<decltype(ulModuleBytes)>
				(hModule);

			IMAGE_NT_HEADERS* lpNtHdr = reinterpret_cast<decltype(lpNtHdr)>
				(ulModuleBytes + 0xE8);
			IMAGE_EXPORT_DIRECTORY* lpExpDir = reinterpret_cast<decltype(lpExpDir)>
				(ulModuleBytes + lpNtHdr->OptionalHeader.DataDirectory[0x0].VirtualAddress);

			unsigned long*	ulFunctions = reinterpret_cast<decltype(ulFunctions)>
				(ulModuleBytes + lpExpDir->AddressOfFunctions);
			unsigned long*	ulNames = reinterpret_cast<decltype(ulNames)>
				(ulModuleBytes + lpExpDir->AddressOfNames);
			unsigned short* uiOrdinals = reinterpret_cast<decltype(uiOrdinals)>
				(ulModuleBytes + lpExpDir->AddressOfNameOrdinals);

			for (long i(0); i <= lpExpDir->NumberOfNames; ++i) {
				if (!wcscmp(lpFunctionName.c_str(),
					reinterpret_cast<const wchar_t*>(hModule) + ulNames[i])) {
					lpAddressOfFunction = reinterpret_cast<decltype(lpAddressOfFunction)>
						(ulModuleBytes + ulFunctions[uiOrdinals[i]]);
					return lpAddressOfFunction;
				}
			}
		}

		return false;
	}*/

#if defined(_UNICODE) && defined(UNICODE) // UNICODE
	#define GetModules GetModulesW
	#define FindModule FindModuleW
	#define GetFunction GetFunctionW
#elif defined (_MBCS) // ANSI
	#define GetModules GetModulesA
	#define FindModule FindModuleA
	#define GetFunction GetFunctionA
#endif
}

#endif